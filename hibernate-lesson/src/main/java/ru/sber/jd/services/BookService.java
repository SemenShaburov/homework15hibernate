package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.sber.jd.entities.BooksEntity;
import ru.sber.jd.entities.ClassificationBooksEntity;
import ru.sber.jd.repositories.ClassificationBooksRepository;
import ru.sber.jd.repositories.BooksRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BookService implements CommandLineRunner {

    private final ClassificationBooksRepository classificationBooksRepository;
    private final BooksRepository booksRepository;

    @Override
    public void run(String... args) throws Exception {

       // List<ClassificationBooksEntity>  entities = new ArrayList<>();
        //UUID uuid = UUID.randomUUID();
        ClassificationBooksEntity classificationBooksEntity = new ClassificationBooksEntity();

        classificationBooksEntity.setId(new Random().nextInt());
        classificationBooksEntity.setCountry("Россия");

       ClassificationBooksEntity save = classificationBooksRepository.save(classificationBooksEntity);

        BooksEntity booksEntity = new BooksEntity();
        booksEntity.setName("Мастер и Маргарита");
        booksEntity.setAuthor("Михаил Булгаков");
        booksEntity.setGenre("роман");
        booksEntity.setISBN("978-5-389-01666-8");

        booksRepository.save(booksEntity);

        System.out.println(classificationBooksRepository.findAll());
        System.out.println(booksRepository.findAll());

    }
}
