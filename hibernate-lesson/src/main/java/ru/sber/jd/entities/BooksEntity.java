package ru.sber.jd.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity(name = "books")
public class BooksEntity {

    @Id
    @Column
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer id;
    private String name;
    @Column
    private String author;
    @Column
    private String genre;
    @Column
    private String ISBN;
}
