package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.ClassificationBooksEntity;

@Repository
public interface ClassificationBooksRepository extends JpaRepository<ClassificationBooksEntity, Integer> {
}
