package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.BooksEntity;

@Repository
public
interface BooksRepository extends JpaRepository<BooksEntity, Integer> {
}
